defmodule RitAPIWeb.Router do
  use RitAPIWeb, :router

  pipeline :graphql do
    plug :accepts, ["graphql", "json"]
    plug :fetch_session
    plug :put_secure_browser_headers
  end

  pipeline :auth do
    plug RitAPIWeb.Guardian.Pipeline
    plug :fetch_user
  end

  pipeline :auth_api do
    plug :fetch_user_for_api
  end

  scope "/" do
    pipe_through [:graphql, :auth, :auth_api]

    forward "/api", Absinthe.Plug, schema: RitAPIWeb.GraphQL.Schema

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: RitAPIWeb.GraphQL.Schema,
      interface: :simple,
      context: %{pubsub: RitAPIWeb.Endpoint}
  end

  def fetch_user(conn, _params) do
    assign(conn, :current_user, Guardian.Plug.current_resource(conn))
  end

  def fetch_user_for_api(%Plug.Conn{assigns: %{current_user: user}} = conn, _params) do
    Absinthe.Plug.put_options(conn, context: %{current_user: user})
  end
end
