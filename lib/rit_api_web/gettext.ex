defmodule RitAPIWeb.Gettext do
  @moduledoc false

  use Gettext, otp_app: :rit_api
end
