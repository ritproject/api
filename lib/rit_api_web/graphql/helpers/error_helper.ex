defmodule RitAPIWeb.GraphQL.ErrorHelper do
  @moduledoc """
  Responsible to handle errors to GraphQL
  """

  alias Ecto.Changeset

  @default_message "Failed to perform the operation"

  def handle_changeset_error(error, message \\ @default_message)

  def handle_changeset_error({:error, error}, message) do
    result = %{message: translate_error(message)}
    {:error, handle({:fields, error}, result)}
  end

  def handle_changeset_error(_error, message) do
    show_error(message)
  end

  def show_error(message \\ @default_message) do
    {:error, translate_error(message)}
  end

  defp handle({key, %Changeset{errors: [], changes: changes}}, result) do
    key_result = Enum.reduce(changes, %{}, &handle/2)

    if Enum.empty?(key_result) do
      result
    else
      Map.put(result, key, key_result)
    end
  end

  defp handle({key, %Changeset{errors: errors}}, result) do
    key_result = Enum.reduce(errors, %{}, &handle/2)

    if Enum.empty?(key_result) do
      result
    else
      Map.put(result, key, key_result)
    end
  end

  defp handle({key, {msg, opts}}, result) do
    Map.put(result, key, [translate_error(msg, opts)] ++ Map.get(result, key, []))
  end

  defp handle(_error, result) do
    result
  end

  defp translate_error(msg, opts \\ []) do
    if count = opts[:count] do
      Gettext.dngettext(RitAPIWeb.Gettext, "errors", msg, msg, count, opts)
    else
      Gettext.dgettext(RitAPIWeb.Gettext, "errors", msg, opts)
    end
  end
end
