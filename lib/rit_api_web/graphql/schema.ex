defmodule RitAPIWeb.GraphQL.Schema do
  @moduledoc false

  use Absinthe.Schema

  alias RitDatabase.Accounts

  import_types(RitAPIWeb.GraphQL.Accounts)

  query do
    import_fields(:accounts_queries)
  end

  mutation do
    import_fields(:accounts_mutations)
  end

  def plugins do
    [Absinthe.Middleware.Dataloader] ++ Absinthe.Plugin.defaults()
  end

  def dataloader do
    Dataloader.new()
    |> Dataloader.add_source(Accounts, Accounts.data())
  end

  def context(ctx) do
    Map.put(ctx, :loader, dataloader())
  end
end
