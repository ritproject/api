defmodule RitAPIWeb.GraphQL.Accounts.Mutations do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias RitAPIWeb.GraphQL.Accounts.Resolvers

  object :accounts_mutations do
    field :create_user, :user do
      arg(:credential, non_null(:create_credential))
      arg(:first_name, non_null(:string))
      arg(:last_name, non_null(:string))
      arg(:username, non_null(:string))

      resolve(&Resolvers.create_user/3)
    end

    field :update_user, :user do
      arg(:id, non_null(:id))
      arg(:first_name, :string)
      arg(:last_name, :string)

      resolve(&Resolvers.update_user/3)
    end

    field :delete_user, :user do
      arg(:id, non_null(:id))

      resolve(&Resolvers.delete_user/3)
    end

    field :login, :user_and_token do
      arg(:email, non_null(:string))
      arg(:password, non_null(:string))

      resolve(&Resolvers.login/3)
    end

    field :signup, :user_and_token do
      arg(:credential, non_null(:create_credential))
      arg(:first_name, non_null(:string))
      arg(:last_name, non_null(:string))
      arg(:username, non_null(:string))

      resolve(&Resolvers.create_user/3)
    end
  end

  input_object :create_credential do
    field :email, non_null(:string)
    field :password, non_null(:string)
  end
end
