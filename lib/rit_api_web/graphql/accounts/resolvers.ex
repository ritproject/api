defmodule RitAPIWeb.GraphQL.Accounts.Resolvers do
  @moduledoc false

  alias Absinthe.Resolution
  alias RitAPI.Guardian
  alias RitAPIWeb.GraphQL.ErrorHelper
  alias RitDatabase.Accounts

  def list_users(_root, _args, _info) do
    {:ok, Accounts.list_users()}
  end

  def get_user(_root, %{id: id}, _info) do
    {:ok, Accounts.get_user!(id)}
  rescue
    _error -> ErrorHelper.show_error("User not found")
  end

  def get_user(_root, _args, %Resolution{context: %{current_user: user}}) do
    if user do
      {:ok, user}
    else
      ErrorHelper.show_error("You are not authenticated")
    end
  end

  def create_user(_root, args, %Resolution{definition: %{name: name}}) do
    case Accounts.create_user(args) do
      {:ok, user} ->
        if name == "signup" do
          {:ok, token, _claims} = Guardian.encode_and_sign(user)
          {:ok, %{user: user, token: token}}
        else
          {:ok, user}
        end

      error ->
        ErrorHelper.handle_changeset_error(error, "Failed to create user")
    end
  end

  def update_user(root, args, info) do
    case get_user(root, args, info) do
      {:ok, user} ->
        case Accounts.update_user(user, args) do
          {:ok, user} -> {:ok, user}
          error -> ErrorHelper.handle_changeset_error(error, "Failed to update user")
        end

      error ->
        error
    end
  end

  def delete_user(root, args, info) do
    case get_user(root, args, info) do
      {:ok, user} ->
        case Accounts.delete_user(user) do
          {:ok, user} -> {:ok, user}
          error -> ErrorHelper.handle_changeset_error(error, "Failed to delete user")
        end

      error ->
        error
    end
  end

  def login(_root, args, _info) do
    user = Accounts.login!(args)
    {:ok, token, _claims} = Guardian.encode_and_sign(user)
    {:ok, %{user: user, token: token}}
  rescue
    _error -> ErrorHelper.show_error("Failed to login")
  end
end
