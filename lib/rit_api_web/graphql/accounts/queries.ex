defmodule RitAPIWeb.GraphQL.Accounts.Queries do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias RitAPIWeb.GraphQL.Accounts.Resolvers

  object :accounts_queries do
    field :users, list_of(:user), resolve: &Resolvers.list_users/3

    field :user, :user do
      arg(:id, non_null(:id))

      resolve(&Resolvers.get_user/3)
    end

    field :profile, :user, resolve: &Resolvers.get_user/3
  end
end
