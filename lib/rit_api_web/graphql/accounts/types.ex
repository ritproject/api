defmodule RitAPIWeb.GraphQL.Accounts.Types do
  @moduledoc false

  use Absinthe.Schema.Notation

  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  alias RitDatabase.Accounts

  object :user do
    field :first_name, non_null(:string)
    field :id, non_null(:id)
    field :last_name, non_null(:string)
    field :username, non_null(:string)

    field :credential, non_null(:credential), resolve: dataloader(Accounts)
  end

  object :credential do
    field :email, non_null(:string)
    field :user, non_null(:user), resolve: dataloader(Accounts)
  end

  object :user_and_token do
    field :user, non_null(:user)
    field :token, non_null(:string)
  end
end
