defmodule RitAPIWeb.GraphQL.Accounts do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias RitAPIWeb.GraphQL

  import_types(GraphQL.Accounts.{Mutations, Queries, Types})
end
