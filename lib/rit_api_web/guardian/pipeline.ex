defmodule RitAPIWeb.Guardian.Pipeline do
  @moduledoc false

  use Guardian.Plug.Pipeline,
    otp_app: :rit_api,
    error_handler: RitAPIWeb.Guardian.ErrorHandler,
    module: RitAPI.Accounts.Guardian

  plug Guardian.Plug.VerifySession, claims: %{"typ" => "access"}
  plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}
  plug Guardian.Plug.LoadResource, allow_blank: true
end
