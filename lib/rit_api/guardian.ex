defmodule RitAPI.Guardian do
  @moduledoc """
  Responsible to generate and translate JWT tokens to users
  """

  use Guardian, otp_app: :rit_api

  alias RitDatabase.Accounts
  alias RitDatabase.Accounts.User

  def subject_for_token(%User{id: id}, _claims) do
    {:ok, to_string(id)}
  end

  def resource_from_claims(%{"sub" => id}) do
    {:ok, Accounts.get_user!(id)}
  rescue
    _error -> {:error, :user_not_found}
  end
end
