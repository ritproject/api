defmodule RitAPI.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      RitAPIWeb.Endpoint
    ]

    opts = [strategy: :one_for_one, name: RitAPI.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
