import Config

config :rit_database, RitDatabase.Repo,
  username: "postgres",
  password: "postgres",
  database: "rit_test",
  hostname: "postgres",
  pool: Ecto.Adapters.SQL.Sandbox
