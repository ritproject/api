import Config

hostname =
  System.get_env("RIT_HOSTNAME") ||
    raise "environment variable RIT_HOSTNAME is missing."

port = String.to_integer(System.get_env("RIT_PORT") || "80")

secret_key_base =
  System.get_env("RIT_SECRET_KEY_BASE") ||
    raise """
    environment variable RIT_SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :rit_api, RitAPIWeb.Endpoint,
  http: [:inet6, port: port],
  url: [host: hostname, port: port],
  cache_static_manifest: "priv/static/cache_manifest.json",
  secret_key_base: secret_key_base

# ## SSL Support
#
# To get SSL working, you will need to add the `https` key
# to the previous section and set your `:url` port to 443:
#
#     config :rit_api, RitAPIWeb.Endpoint,
#       ...
#       url: [host: "example.com", port: 443],
#       https: [
#         :inet6,
#         port: 443,
#         cipher_suite: :strong,
#         keyfile: System.get_env("SOME_APP_SSL_KEY_PATH"),
#         certfile: System.get_env("SOME_APP_SSL_CERT_PATH")
#       ]
#
# The `cipher_suite` is set to `:strong` to support only the
# latest and more secure SSL ciphers. This means old browsers
# and clients may not be supported. You can set it to
# `:compatible` for wider support.
#
# `:keyfile` and `:certfile` expect an absolute path to the key
# and cert in disk or a relative path inside priv, for example
# "priv/ssl/server.key". For all supported SSL configuration
# options, see https://hexdocs.pm/plug/Plug.SSL.html#configure/1
#
# We also recommend setting `force_ssl` in your endpoint, ensuring
# no data is ever sent via http, always redirecting to https:
#
#     config :rit_api, RitAPIWeb.Endpoint,
#       force_ssl: [hsts: true]
#
# Check `Plug.SSL` for all available options in `force_ssl`.

guardian_secret_key =
  System.get_env("RIT_GUARDIAN_SECRET_KEY") ||
    raise """
    environment variable RIT_GUARDIAN_SECRET_KEY is missing.
    You can generate one by calling: mix guardian.gen.secret
    """

config :rit_api, RitAPI.Accounts.Guardian, secret_key: guardian_secret_key
