import Config

config :rit_database, RitDatabase.Repo,
  username: "postgres",
  password: "postgres",
  database: "rit_dev",
  hostname: "postgres",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10
