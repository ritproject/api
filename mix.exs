defmodule RitAPI.MixProject do
  use Mix.Project

  def project do
    [
      app: :rit_api,
      version: "0.0.1",
      elixir: "~> 1.9",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test,
        "test.coverage": :test,
        "test.static": :test
      ],
      releases: [
        rit_api: [
          include_executables_for: [:unix],
          include_erts: false
        ]
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {RitAPI.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:absinthe_plug, "~> 1.4.7"},
      {:absinthe, "~> 1.4.16"},
      {:comeonin, "~> 5.1.2"},
      {:credo, "~> 1.1.2", only: [:dev, :test], runtime: false},
      {:dataloader, "~> 1.0.6"},
      {:excoveralls, "~> 0.11.1", only: [:dev, :test]},
      {:gettext, "~> 0.11"},
      {:guardian, "~> 2.0.0"},
      {:jason, "~> 1.0"},
      {:phoenix_ecto, "~> 4.0"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix, "~> 1.4.9"},
      {:plug_cowboy, "~> 2.0"},
      {:rit_database, "~> 0.0.1"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.bootstrap": ["ecto.create", "ecto.migrate"],
      "ecto.setup": ["ecto.bootstrap", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "test.coverage": ["ecto.drop", "ecto.bootstrap", "coveralls"],
      "test.static": ["credo list --strict --all"]
    ]
  end
end
